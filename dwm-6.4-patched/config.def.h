/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Firacode:size=11", "FontAwesome:size=12", "Noto Color Emoji:size=14"};
static const char dmenufont[]       = "Firacode:size=11";
static const char col_gray1[]       = "#458588";
static const char col_gray2[]       = "#E5E9F0";
static const char col_gray3[]       = "#d5c4a1";
static const char col_gray4[]       = "#7c6f64";
static const char co_backgr[]       = "#282828";
static const char col_cyan[]        = "#d65d0e";
static const char col_sel_border[]  = "#966f23";
//static const char col_gray3[]       = "#ECEFF4"; NORD
// static const char col_gray4[]       = "#4C566A"; NORD
// static const char col_cyan[]        = "#BF616A";
// static const char co_backgr[]       = "#2e3440"; NORD
// static const char col_sel_border[]  = "#D08770"; NORD
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, co_backgr, col_gray4 },
	[SchemeSel]  = { col_cyan, co_backgr,  col_sel_border  },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "", "", "", "", ""};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static Rule rules[] ={
	{"tdrop",NULL,NULL,~0,1,-1},
	{"thunderbird",NULL,NULL,1 << 6,1,-1},
	{"Microsoft Teams - Preview",NULL,NULL,1 << 12 ,0,-1},
	{"Signal",NULL,NULL,1 << 11 ,0,-1},
	{"mpv",NULL,NULL,1 << 8 ,0,-1}
	
};
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "",         tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", co_backgr, "-nf", col_gray3, "-sb", col_cyan, "-sf", co_backgr, NULL };
static const char *termcmd[]  = { "alacritty", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY, 	                XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_p,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_dead_diaeresis,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_dead_diaeresis,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_uacute,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_parenright, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_uacute,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_parenright, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_plus,                        0)
	TAGKEYS(                        XK_ecaron,                      1)
	TAGKEYS(                        XK_scaron,                      2)
	TAGKEYS(                        XK_ccaron,                      3)
	TAGKEYS(                        XK_rcaron,                      4)
	TAGKEYS(                        XK_zcaron,                      5)
	TAGKEYS(                        XK_yacute,                      6)
	TAGKEYS(                        XK_aacute,                      7)
	TAGKEYS(                        XK_iacute,                      8)
	TAGKEYS(                        XK_eacute,                      9)
	TAGKEYS(                        XK_equal,                      10)
	TAGKEYS(                        XK_dead_acute,                 11)
	TAGKEYS(                        XK_semicolon,                  12)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

