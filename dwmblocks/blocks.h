//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "music status",					20,		0},
	{"|", "echo ''",					60000,		0},
	{"", "curl -m 10 -s wttr.in/Edmonton?format=3| sed 's/Edmonton: //g'",					600,		0},
	{" ", "sensors | grep Package | cut -d ' ' -f 5 | sed 's/\\+//g'",					15,		0},
	{" ", "cat /sys/class/backlight/intel_backlight/brightness*|cut -c 1-2| sed 's/$/%/'",					60,		0},
	{" ", "amixer | grep -o -E '[[:digit:]]+%' | head -n 1",					60,		0},
	{" ", "free --mega | grep Mem | awk '{print $3/($2/100)}'| cut -c -4 | xargs echo -n && echo -n '%'",					60,		0},
	{" ", "df -h / | grep root | awk '{print $4}'",					600,		0},
	{"", "grep Charging /sys/class/power_supply/BAT0/status &>/dev/null && echo -n ''",					30,		0},
	{" ", "cat /sys/class/power_supply/BAT0/capacity && echo '%'",					30,		0},
	{"", "connected_devices",					60,		0},
	{"", "date '+%H:%M'",					10,		0},
	{" ", "date '+%d' && echo "  "",					600,		0},
	{"", "echo ''",					6000000,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 5;
